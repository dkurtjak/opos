/**
 * File:   list.c
 * Author: Daniel Kurtjak
 *
 * Created on June 24, 2015, 1:17 PM
 */

#include <stdlib.h>
#include "list.h"

const unsigned char RESOURCE_1 = 0x01;
const unsigned char RESOURCE_2 = 0x02;

void initNode(node_t *n, uint16_t l1, uint16_t l2)
{
    n->next = NULL;
    n->filledResources = 0;
    n->length1 = l1;
    n->length2 = l2;
    n->mask = (l1 ? RESOURCE_1 : 0) | (l2 ? RESOURCE_2 : 0);
    pthread_mutex_init(&n->mutex, NULL);
    pthread_cond_init(&n->cond, NULL);
}

void push(list_t* q, node_t* n)
{
    node_t* t = q->first;
    if (t == NULL)
    {
        q->first = n;
    }
    else
    {
        while (t->next != NULL)
        {
            t = t->next;
        }
        t->next = n;
    }
}

void removeNode(list_t* q, node_t* n)
{
    if (n == q->first)
    {
        q->first = q->first->next;
    }
    else
    {
        node_t* tmp = q->first;
        while (tmp->next != n)
        {
            tmp = tmp->next;
        }
        tmp->next = tmp->next->next;
    }
}
