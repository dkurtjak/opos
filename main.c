/** 
 * File:   main.c
 * Author: Daniel Kurtjak
 *
 * The main idea is to make some FIFO structure (single linked list) and use it to synchronize client's requests with 
 * servers responses. There are two buffers R1 and R2 which represents required resources. Communication begins in a way
 * that client sends request in a form of `node_t` data which contains information how much of resource 1 and how much of
 * resource 2 is required. The information about how much resource is requested by client is send through length1 and 
 * length2 fields. Requested resource will be returned to the client using data1 or data2 buffers.
 * There are two threads that refill particular resource buffers, fillR1Task and fillR2Task for R1 and R2 resource
 * respectively.
 *
 * Created on June 12, 2015, 2:22 PM
 *
 * Zadatak:
 * Написати вишенитни програм у којем једна нит опслужује пристигле захтеве
 * других нити за одређеном количином ресурса R1 и R2.
 * Нит клијент може да захтева одређену количину ресурса по следећа три
 * сценарија:
 * - клијент захтева одређену количину само ресурса R1,
 * - клијент захтева одређену количину само ресурса R2 ,
 * - клијент захтева одређену количину оба ресурса R1 и R2.
 * Ресурс R1 се периодично допуњује сваких RELOAD_TIME милисекунди са
 * одређеном количином LOAD_R1. Ресурс R2 се допуњује одређеном количином
 * LOAD_R2, на захтев нити која опслужује клијенте и то када се ресурс R2 потроши.
 * Водити рачуна да уколико нпр. клијент који захтева одређену количину оба
 * ресурса и чека на допуњавање једног он њих, да остали клијенти који потражују ресурс
 * којег има буду послужени. Исто тако водити рачуна да клијенти који први потражују
 * одређени ресурс буду и први послужени њиме.
 *
 *
 * Rjesenje:
 * Solution is implemented in a way that there are two buffers R1 and R2 that represent required resources.
 *
 * The main idea is to make a FIFO structure (single linked list) and use it to synchronize client's requests with 
 * server's responses. Communication begins in a way that client sends request in a form of `node_t` data which contains 
 * information about how much of each resource is required and it is sent through length1 and length2 fields. 
 *
 * Requested resource is returned to the client using data1 or data2 buffers.
 *
 * There are two threads that are used to refill particular resource buffers, fillR1Task and fillR2Task for R1 and R2
 * resource respectively.
 */

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include "list.h"

#define DEBUG_DUMPS

//Just one helper structure to encapsulate data that will be passed to client thread
typedef struct
{
    list_t* requestList;
    node_t node;
    uint32_t consumerSleep;
} client_data_t;

static const uint16_t RELOAD_TIME = 100;

static const uint16_t LOAD_R1 = 10;
static const uint16_t LOAD_R2 = 20;

//Buffer size for resource 1
#define BUFFER_R1_SIZE 100
//Buffer size for resource 2
#define BUFFER_R2_SIZE 100

//Resource R1 buffer
static rest_type_t R1[BUFFER_R1_SIZE];
//Resource R2 buffer
static rest_type_t R2[BUFFER_R2_SIZE];

//R1 and R2 are circular buffers
//r1Start and r2Start are start position from R1 and R2 respectively
static uint16_t r1Start = 0;
static uint16_t r2Start = 0;

//r1Total and r2Total are total size of R1 and R2 respectively they should be less then BUFFER_R1_SIZE and BUFFER_R2_SIZE respectively
static uint16_t r1Total = 0;
static uint16_t r2Total = 0;

//Used to lock R1
static pthread_mutex_t r1mutex = PTHREAD_MUTEX_INITIALIZER;
//Used to lock R2
static pthread_mutex_t r2mutex = PTHREAD_MUTEX_INITIALIZER;
//Used in signal function which reloads R2
static pthread_cond_t r2reloadCond = PTHREAD_COND_INITIALIZER;

//This semaphore indicate whether there is at least one request in request list
static sem_t serverSemaphore;

/**
 * Fill out R1 with LOAD_R1 amount buffer every RELOAD_TIME milliseconds
 * 
 * @param ptr - not used
 */
static void fillR1Task(void *ptr)
{
    uint16_t counter = 0;
    while (1)
    {
        pthread_mutex_lock(&r1mutex);
        counter = 0;
        while (r1Total < BUFFER_R1_SIZE && counter < LOAD_R1)
        {
            R1[(r1Start + r1Total) % BUFFER_R1_SIZE] = counter;
            r1Total++;
            counter++;
        }
        pthread_mutex_unlock(&r1mutex);
        usleep(RELOAD_TIME * 1000);
    }
}

/**
 * Fill out R2 with LOAD_R2 amount buffer on demand
 * 
 * @param ptr - not used
 */
static void fillR2Task(void *ptr)
{
    uint16_t counter = 0;
    while (1)
    {
        pthread_mutex_lock(&r2mutex);
        counter = 0;
        pthread_cond_wait(&r2reloadCond, &r2mutex);
        while (r2Total < BUFFER_R2_SIZE && counter < LOAD_R2)
        {
            R2[(r2Start + r2Total) % BUFFER_R2_SIZE] = counter;
            r2Total++;
            counter++;
        }
        pthread_mutex_unlock(&r2mutex);
    }
}

/**
 * @param resourceType which resource type to copy RESOURCE_1 or RESOURCE_2
 * @param n node
 * @return 1 if all requested resources are copied otherwise 0 - for example if client request both resources and  
 * RESOURCE_1 is filled previously and now, with this call RESOURCE_2 is filled job is completed and 1 will be returned
 */
static uint8_t copyBuffers(int resourceType, node_t *n)
{
    pthread_mutex_lock(&n->mutex);
    rest_type_t* src = (resourceType == RESOURCE_1 ? R1 : R2);
    rest_type_t* dest = (resourceType == RESOURCE_1 ? n->data1 : n->data2);
    uint16_t* total = (resourceType == RESOURCE_1 ? &r1Total : &r2Total);
    uint16_t* start = (resourceType == RESOURCE_1 ? &r1Start : &r2Start);
    uint16_t length = (resourceType == RESOURCE_1 ? n->length1 : n->length2);
    uint16_t bufferSize = (resourceType == RESOURCE_1 ? BUFFER_R1_SIZE : BUFFER_R2_SIZE);
    uint16_t i;
    int ret = 0;

    unsigned char mask = 0;
    for (i = 0; i < length; i++)
    {
        dest[i] = src[(*start + i) % bufferSize];
    }
    *start = (*start + length) % bufferSize;
    *total -= length;
    n->filledResources |= resourceType;
    if (n->filledResources == n->mask)
    {
        pthread_cond_signal(&n->cond);
        ret = 1;
    }
    pthread_mutex_unlock(&n->mutex);
    return ret;
}

/**
 * Server thread, used to serve client requests for requested type(s) of resource(s)
 * 
 * @param ptr - not used
 */
static void serverTask(void *ptr)
{
    list_t* reqList = (list_t *) ptr;
    node_t* current;
    char clientFullyServed;
    while (1)
    {
        //At this point server waits for clients requests. When at least one request is in the list server iterates 
        //through that list and serve client that could be served. For that purpose semaphore is used as request counter.
        //But if none of the client could be served at the end of the inner while loop semaphore is signalized by server
        //itself. When the first client's request is fully served inner while is broken (with break) and semaphore will 
        //remain decremented indicating that exact one client's request is served. Then server iterate through list
        //(FIFO structure) from begin again.
        sem_wait(&serverSemaphore);
        clientFullyServed = 0;
        current = reqList->first;
        while (current != NULL)
        {
            //Manage request of current list node for resource R1
            pthread_mutex_lock(&r1mutex);
            if (current->length1 && !(current->filledResources & RESOURCE_1) && current->length1 <= r1Total)
            {
                if (copyBuffers(RESOURCE_1, current))
                {
                    //After coping this resource to client if request is fully served, that client
                    //request (node that represents request) is removed from list and further iteration
                    //to find client that could be fully served is stopped thus break of inner while loop
                    //is performed.
                    removeNode(reqList, current);
                    clientFullyServed = 1;
                    pthread_mutex_unlock(&r1mutex);
                    break;
                }
            }
            pthread_mutex_unlock(&r1mutex);

            pthread_mutex_lock(&r2mutex);
            //Manage request of current list node for resource R2
            if (current->length2 && !(current->filledResources & RESOURCE_2))
            {
                if (r2Total < current->length2)
                {
                    pthread_cond_signal(&r2reloadCond);
                }
                else
                {
                    if (copyBuffers(RESOURCE_2, current))
                    {
                        //After coping this resource to client if request is fully served, that client
                        //request (node that represents request) is removed from list and further iteration
                        //to find client that could be fully served is stopped thus break of inner while loop
                        //is performed.
                        removeNode(reqList, current);
                        clientFullyServed = 1;
                        pthread_mutex_unlock(&r2mutex);
                        break;
                    }
                }
            }
            pthread_mutex_unlock(&r2mutex);

            current = current->next;
        }

        //If non of the client requests are completely served server increments its semaphore
        if (!clientFullyServed) {
            sem_post(&serverSemaphore);
        }
    }

    pthread_exit(0);
}

/**
 * Client thread that asks for some amount of certain resource every `slp` milliseconds
 * 
 * @param ptr - structure that contains node_t structure witch encapsulate buffers for requested resource and 
 * for length - amount for that particular resource
 */
static void clientTask(void *ptr)
{
    client_data_t* cd = (client_data_t *) ptr;
    list_t* reqList = cd->requestList;
    uint32_t slp = cd->consumerSleep;
    node_t* node = &cd->node;
    int i;
    while (1)
    {
        //lock node to reset nodes pointer and filled resources flag
        pthread_mutex_lock(&node->mutex);
        node->next = NULL;
        node->filledResources = 0;

        //push node in request list
        push(reqList, node);

        //notify server about new node i it
        sem_post(&serverSemaphore);

        //wait for server (copyBuffer function) to complete coping buffers
        pthread_cond_wait(&node->cond, &node->mutex);

        pthread_mutex_unlock(&node->mutex);
        //sleep some time
        usleep(slp * 1000);

#ifdef DEBUG_DUMPS
        if (node->length1)
        {
            printf("R1: ");
            for (i = 0; i < node->length1; i++)
            {
                printf("%d, ", node->data1[i]);
            }
            printf("\n");
        }
        if (node->length2)
        {
            printf("R2: ");
            for (i = 0; i < node->length2; i++)
            {
                printf("%d, ", node->data2[i]);
            }
            printf("\n");
        }
        printf("\n");
#endif
    }
    pthread_exit(0);
}

/**
 * Mentioned FIFO structure - single linked list
 * Used by clients to put their request in it and by server to serve client from it in order i which requests are 
 * generated
 *
 * @var global request list
 */
static list_t requestList;


/**
 * Main function
 */
int main(int argc, char** argv)
{
 /* thread variables */
    pthread_t serverThread;
    pthread_t clientR1Thread;
    pthread_t clientR2Thread;
    pthread_t clientR1R2Thread;
    pthread_t reloadR1Thread;
    pthread_t reloadR2Thread;
 
/* client data variables*/
    client_data_t cdR1;
    client_data_t cdR2;
    client_data_t cdR1R2;

    for (r1Total = 0, r2Total = 0; r1Total < 50; r1Total++, r2Total++)
    {
        R1[r1Total] = r1Total;
        R2[r2Total] = r2Total;
    }

    requestList.first = NULL;
    pthread_mutex_init(&requestList.mutex, NULL);

    sem_init(&serverSemaphore, 0, 0);

    //client1 request only R1
    initNode(&cdR1.node, 10, 0);
    //client2 request only R2
    initNode(&cdR2.node, 0, 20);
    //client3 request R1 and R2
    initNode(&cdR1R2.node, 15, 17);

    cdR1.requestList = &requestList;
    cdR2.requestList = &requestList;
    cdR1R2.requestList = &requestList;

    cdR1.consumerSleep = 1000;
    cdR2.consumerSleep = 2000;
    cdR1R2.consumerSleep = 1500;

    pthread_create(&serverThread, NULL, (void *) &serverTask, (void *) &requestList);

    pthread_create(&reloadR1Thread, NULL, (void *) &fillR1Task, (void *) NULL);
    pthread_create(&reloadR2Thread, NULL, (void *) &fillR2Task, (void *) NULL);

    pthread_create(&clientR1Thread, NULL, (void *) &clientTask, (void *) &cdR1);
    pthread_create(&clientR2Thread, NULL, (void *) &clientTask, (void *) &cdR2);
    pthread_create(&clientR1R2Thread, NULL, (void *) &clientTask, (void *) &cdR1R2);

    //destroy all threads
    pthread_join(serverThread, NULL);

    pthread_join(reloadR1Thread, NULL);
    pthread_join(reloadR2Thread, NULL);

    pthread_join(clientR1Thread, NULL);
    pthread_join(clientR2Thread, NULL);
    pthread_join(clientR1R2Thread, NULL);

    pthread_cond_destroy(&r2reloadCond);

    sem_destroy(&serverSemaphore);

    pthread_cond_destroy(&cdR1.node.cond);
    pthread_cond_destroy(&cdR2.node.cond);
    pthread_cond_destroy(&cdR1R2.node.cond);

    pthread_mutex_destroy(&cdR1.node.mutex);
    pthread_mutex_destroy(&cdR2.node.mutex);
    pthread_mutex_destroy(&cdR1R2.node.mutex);

    return (EXIT_SUCCESS);
}
