/**
 * File:   list.h
 * Author: Daniel Kurtjak
 *
 * Created on June 24, 2015, 1:17 PM
 */

#ifndef LIST_H
#define	LIST_H

#include <stdint.h>
#include <pthread.h>

typedef char rest_type_t;

#define MAX_DATA1_LENGTH 500
#define MAX_DATA2_LENGTH 500

extern const unsigned char RESOURCE_1;
extern const unsigned char RESOURCE_2;

typedef struct _node_t
{
    //Used to implement sigle linked list;
    struct _node_t* next;
    //Indicate whether this resource is filled or not
    //uses RESOURCE_X to indicate whether RESOURCE_X is copied
    unsigned char filledResources;
    //If there is request only for resource 1 that it is RESOURCE_1, if request is only for resource 2 that 
    //it is RESOURCE_2, otherwise it is RESOURCE_1 | RESOURCE_2 - this variable is used in combination with 
    //filledResources to perform check if all requested resources are copied
    unsigned char mask;
    //If request R1 then length1 > 0 otherwise 0
    uint16_t length1;
    //If request R2 then length2 > 0 otherwise 0
    uint16_t length2;
    //In this buffer resource 1 is copied
    rest_type_t data1[MAX_DATA1_LENGTH];
    //In this buffer resource 2 is copied
    rest_type_t data2[MAX_DATA2_LENGTH];
    
    //used for synchronization
    pthread_cond_t cond;
    pthread_mutex_t mutex;
} node_t;

typedef struct
{
    //First node
    node_t* first;
    //used for synchronization
    pthread_mutex_t mutex;
} list_t;

/**
 * Push node 'n' into queue 'q'
 * 
 * @param q queue
 * @param n node
 */
void push(list_t* q, node_t* n);

/**
 * Remove node from queue
 * 
 * @param q queue
 * @param n node
 */
void removeNode(list_t* q, node_t* n);

/**
 * Init node
 * 
 * @param n node
 * @param l1 length 1, request for resource 1
 * @param l2 length 2, request for resource 2
 */
void initNode(node_t *n, uint16_t l1, uint16_t l2);

#endif	/* LIST_H */
